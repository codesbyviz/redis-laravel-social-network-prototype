<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RedisAuth;
class RedisAuthController extends Controller
{

    public function login(Request $request)
    {
        $rules = ['email'=>"required|email",'password'=>"required|string"];
        $request->validate($rules);
        $auth  = new RedisAuth;
        $login = $auth->login($request->email,$request->password);

        
        if($auth->isLoggedIn){
            return redirect()->route('home');
        }
        else{
            return redirect()->back()->withErrors($auth->errors);
        }
    }
    public function logout(Request $request)
    {
        $auth  = new RedisAuth;
        $logout = $auth->logout();
        if($logout){
            return redirect()->route('login')->with(['success'=>"Logged Out"]);
        }
    }
    public function register(Request $request)
    {
        $rules = [
            'name'=>"required|string|max:255",
            'email'=>"required|string|email|max:255",
            'password'=>"required|string|min:8|confirmed"
        ];
        $request->validate($rules);
        $user = new RedisAuth;
        $user->name         = $request->name;
        $user->email        = $request->email;
        $user->password     = $request->password;
        $register = $user->register();
        if($register == true){
            return redirect()->route('home');
        }
        else{
            return redirect()->back()->withErrors($user->errors)->withInput();
        }
    }
}
