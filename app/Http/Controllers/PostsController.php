<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use AppAuth;
use Facades\App\Post;
class PostsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('authentication');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = AppAuth::auth();
        $posts = Post::get();
        return view('posts.index',['posts'=>$posts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        for ($i=0; $i < 100; $i++) { 
            $posts = Post::demo();
        }
        return redirect()->back()->with(['success'=>"Dummy Created."]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(['post'=>"required|string"]);
        $postsave = Post::saves(AppAuth::auth()->email,$request->post);
        if($postsave){
            return redirect()->back()->with(['success'=>"Post Created."]);
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $posts
     * @return \Illuminate\Http\Response
     */
    public function show($post)
    {
        $post  = Post::find($post);
        $user = Post::user($post);
        return $user;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $posts
     * @return \Illuminate\Http\Response
     */
    public function edit(Posts $posts)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Posts  $posts
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $post  = Post::updates($id,$request->post);
        if($post == "OK"){
            return redirect()->back()->with(['success'=>"Post Updated."]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Posts  $posts
     * @return \Illuminate\Http\Response
     */
    public function destroy(Posts $posts)
    {
        //
    }
}
