<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Redis;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Database\Eloquent\Collection;
class Post extends Model
{
    public $created_at,$updated_at,$user,$content,$likes;

    public function get()
    {   
        // for ($i=0; $i < 300; $i++) { 
        //     $this->saves();
        // }
        $values = Redis::lrange('posts', 0, -1);
        $data  = [];
        foreach ($values as $key => $value) {
            $data[$key] = json_decode($value,true);
            $data[$key]['user'] = $this->user($data[$key]);
        }
        return collect($data);
    }
    public function user($post)
    {       
        $user =  Redis::get('user:'.$post['user']);
        return json_decode($user,true);
    }
    public function find($post)
    {       
        $post =  Redis::lrange('posts', $post, $post);
        if($post){
            return json_decode($post[0],true);
        }
        else{
            abort(404);
        }
    }
        /**
        *
        * @param array|Collection      $items
        * @param int   $perPage
        * @param int  $page
        * @param array $options
        *
        * @return LengthAwarePaginator
        */
        public function paginate($perPage = 15, $page = null, $options = [])
        {
            $items = $this->get();
            $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
            $items = $items instanceof Collection ? $items : Collection::make($items);
            return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
        }
    public function demo()
    {   
        $sentences = array(
            [
                "Those earphones don't work.",
                "We're no worse off than we were before.",
                "He pulled the wounded soldier to the nearby bush.",                
                "Tom had a dog on a short leash.",
                "A man who never makes mistakes is a man who does nothing.",
                "I intend to do that for you.",
                "They give good service at that restaurant.",
                "I do not like music.",
                "When a weasel pays a new year visit to a chicken, that's no reason for comfort!",
            ],
        );
        Redis::lpush('posts', 
            json_encode(
                ['created_at'=>now(),
                "updated_at"=>now(),
                "user"=>"synamatics@gmail.com",
                "content"=>$sentences[0][rand(0, 8)] ." ". $sentences[0][rand(0, 8)]." ".$sentences[0][rand(0, 8)],
                "likes"=>0])
            );
        
    }
    public function saves($email,$content)
    {   
       if($email && $content){
           Redis::lpush('posts', 
               json_encode(
                   ['created_at'=>now(),
                   "updated_at"=>now(),
                   "user"=>$email,
                   "content"=>$content,
                   "likes"=>0])
               );
            return true;
       }
       else{
           return false;
       }
        
    }
    public function updates($id,$content)
    {   
        $post = $this->find($id);
        $post['content'] = $content;
        $post['updated_at'] = now();
        $cont = Redis::lset('posts', $id,json_encode($post));
        return $cont;
    }

}
